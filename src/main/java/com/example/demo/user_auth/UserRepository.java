package com.example.demo.user_auth;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,String> {


    public User findUserByPasswordAndUid(String pwd,String uid);


}
