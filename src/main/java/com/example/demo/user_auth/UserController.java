package com.example.demo.user_auth;


import com.example.demo.Response_status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;


    //private Response_status response_status;


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST,value = "/loginDetail")
    public Response_status login(@RequestBody User user){
        if(userRepository.findUserByPasswordAndUid(user.getPassword(),user.getUid())!=null){
            //System.out.print("success");
            return new Response_status("success");
        }
        //System.out.print("wrong data");
        return new Response_status("wrong data");
    }


}
