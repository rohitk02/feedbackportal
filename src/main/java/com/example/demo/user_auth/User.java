package com.example.demo.user_auth;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class User {

    private String uid;
    private String password;

    public User(String uid, String password) {
        this.uid=uid;
        this.password=password;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
