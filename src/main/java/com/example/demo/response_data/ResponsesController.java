package com.example.demo.response_data;


import com.example.demo.Response_status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ResponsesController {

    @Autowired
    private ResponsesRepository responsesRepository;

    ///for submitting form response
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST,value = "/addresponse")
    public Response_status addResponse(@RequestBody Responses responses){
        if(responsesRepository.insert(responses)!=null){
            System.out.print("success");
            return new Response_status("success");
        }
        return new Response_status("failed to insert");
    }

    //get all responses of particular form
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET,value="/getresponses/{form_id}")
    public Optional<Responses[]> getResponses(@PathVariable String form_id){
        return responsesRepository.getResponsesByForm_id(form_id);
    }



}
