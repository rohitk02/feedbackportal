package com.example.demo.response_data;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Responses")
public class Responses {
    private String form_id;
    private String user_id;
    private Answers[] answers;

    public Answers[] getAnswers() {
        return answers;
    }

    public void setAnswers(Answers[] answers) {
        this.answers = answers;
    }



    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }




}
