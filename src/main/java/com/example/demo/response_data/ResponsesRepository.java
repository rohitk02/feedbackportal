package com.example.demo.response_data;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface ResponsesRepository extends MongoRepository<Responses,String> {

    @Query(value = "{'form_id':?0}")
    public Optional<Responses[]> getResponsesByForm_id(String form_id);


    //public boolean insertResponse(Responses response);

}
