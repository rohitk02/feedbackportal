package com.example.demo.fom_format_data;


import com.example.demo.Response_status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class FormController {

    @Autowired
    private FormServices formService;


    ////////getting form data from form id
    @CrossOrigin
    @RequestMapping("/singleform/{id}")
    public Optional<FormDocument> getForm(@PathVariable String id){
        return formService.getForm(id);
    }


    ///////creating a new form
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST,value = "/form")
    public Response_status insertForm(@RequestBody FormDocument f){
        if(formService.insertForm(f)){
            return new Response_status("success");
        }
        return new Response_status("failed");
    }

    ////////getting forms using user's id
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET,value = "/form/{uid}")
    public Optional<FormDocument[]> getAllForms(@PathVariable String uid){
        return formService.getAllForms(uid);
    }

    //////Deleting form using form id
    @CrossOrigin
    @RequestMapping("/deleteForm/{form_id}")
    public Response_status deleteForm(@PathVariable String form_id){
        if(formService.deleteForm(form_id))
            return new Response_status("success");
        return new Response_status("failed");
    }
}
