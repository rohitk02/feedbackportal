package com.example.demo.fom_format_data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface FormRepository extends MongoRepository<FormDocument,String> {

    //public FormDocument[] findAllByUser_id(String uid);

    ////find all forms for particular user
    @Query(value = "{'user_id':?0}",fields = "{ id:1 , name:1 }")
    public Optional<FormDocument[]> findByUser_id(String uid);

    //public Optional<FormDocument[]> findAllByUser_idEquals(String uid);
}
