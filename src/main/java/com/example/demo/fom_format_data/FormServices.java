package com.example.demo.fom_format_data;


import com.example.demo.Response_status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FormServices {

    @Autowired
    private FormRepository formRepository;

    public Optional<FormDocument> getForm(String id){
        return formRepository.findById(id);
    }

    public boolean insertForm(FormDocument f){
        if (formRepository.insert(f)!=null)
            return true;
        else{
            return false;
        }


    }

    public boolean deleteForm(String form_id){
        formRepository.deleteById(form_id);
        return true;
    }

    public Optional<FormDocument[]> getAllForms(String uid) {
        return formRepository.findByUser_id(uid);
    }
}
